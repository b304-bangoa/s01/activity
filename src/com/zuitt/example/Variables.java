package com.zuitt.example;

public class Variables {

    public static void main(String[] args) {

        //Declaration of Identifiers
        int age;
        char middleName;

        // Variable Declaration vs Initialization
        int x;
        int y = 0;

        x = 1;

        System.out.println("The value of y is " + y + " and the value of x is " + x);

        //Primitive data types
        int wholeNumber = 100;
        System.out.println(wholeNumber);
        //Long
        long worldPopulation = 78628811457878L;
        System.out.println(worldPopulation);
        //Float
        float piFloat = 3.14159265359f;
        System.out.println(piFloat);
        //Double
        double piDouble = 3.14159265359;
        System.out.println(piDouble);
        //Char
        char letter = 'a';
        System.out.println(letter);
        //Boolean
        boolean isLove = true;
        boolean isTaken = false;
        System.out.println(isLove);
        System.out.println(isTaken);
        //Constant
        final int PRINCIPAL = 3000;
        System.out.println(PRINCIPAL);

        //Non-Primitive data type
        //String
        String username = "JSmith";
        System.out.println(username);
        //Sample string method
        int stringLength = username.length();
        System.out.println(stringLength);


    }

}
