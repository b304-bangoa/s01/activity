package com.zuitt.wdc0043;

import java.util.Scanner;

public class User {

    public static void main(String[] args){

        String firstName;
        String lastName;
        double firstSubject;
        double secondSubject;
        double thirdSubject;


        Scanner userScanner = new Scanner(System.in);

        System.out.println("Enter your First name: ");
        firstName = userScanner.next();
        System.out.println("Enter your Last name: ");
        lastName = userScanner.next();
        System.out.println("First Subject Grade: ");
        firstSubject = userScanner.nextDouble();
        System.out.println("Second Subject Grade: ");
        secondSubject = userScanner.nextDouble();
        System.out.println("Third Subject Grade: ");
        thirdSubject = userScanner.nextDouble();

        System.out.println("Good day, " + firstName + " " + lastName + "." );
        double avg = (firstSubject + secondSubject + thirdSubject) / 3;
        int roundedAvg = (int) avg ;
        System.out.println("Your grade average is: " + roundedAvg);

    }
}
